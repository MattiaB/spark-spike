import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.StreamingContext._

object SocketSparkStreaming {
  def main(args: Array[String]): Unit = {
    val ssc = new StreamingContext(new SparkConf(), Seconds(1))
    ssc.checkpoint("checkpoint")
    // Create a DStream that will connect to hostname:port, like localhost:9999
    val lines = ssc.socketTextStream("localhost", 9999)
    // Split each line into words
    val words = lines.flatMap(_.split(" "))

    // Count each word in each batch
    val pairs = words.map(word => (word, 1))
    //val wordCounts = pairs.reduceByKey(_ + _)
    val wordCounts = pairs.reduceByKeyAndWindow((a:Int,b:Int)=>a+b, Seconds(30), Seconds(1))

    // Print the first ten elements of each RDD generated in this DStream to the console
    wordCounts.print()
    ssc.start()             // Start the computation
    ssc.awaitTermination()  // Wait for the computation to terminate
  }

}
