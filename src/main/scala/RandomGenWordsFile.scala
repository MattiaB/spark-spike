import java.io.PrintStream
import java.net.ServerSocket

import scala.util.Random


object RandomGenWordsFile {
  def main(args: Array[String]): Unit = {
    val totWords = args(0).toInt
    val words = args(1).split(",").toArray
    val server = new ServerSocket(9999)
    val s = server.accept()
    val out = new PrintStream(s.getOutputStream)
    for( a <- 1 to totWords){
      out.println(
        words(
          Random.nextInt(words.length)
        )
      )
      out.flush()
    }
    out.close()
    s.close()
  }
}
