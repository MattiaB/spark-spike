import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._

object TwitterSparkStreaming {
  def main(args: Array[String]) {
    // Complete tutorial
    // https://databricks-training.s3.amazonaws.com/realtime-processing-with-spark-streaming.html
    // http://spark.apache.org/docs/latest/streaming-programming-guide.html
    // Configure Twitter credentials
    val apiKey = "9drk6EWh2BCIsECyBXvA0g"
    val apiSecret = "ed30t273MkiWDp0LI6BQCddCn5qixZ8ypLvqnDwpMo"
    val accessToken = "27909705-3No1C2kwwxwyhJb8YuoitwPcimXpRcKhYnnmCMJ4O"
    val accessTokenSecret = "EhU9Sh1xP22wUymrfDWtmqRlvCHqVw8NkZcgPHiJXoTcT"
    Helper.configureTwitterCredentials(apiKey, apiSecret, accessToken, accessTokenSecret)

    val ssc = new StreamingContext(new SparkConf(), Seconds(1))
    ssc.checkpoint("checkpoint")
    val tweets = TwitterUtils.createStream(ssc, None)
    val statuses = tweets.map(status => status.getText)
    val words = statuses.flatMap(status => status.split(" "))
    val hashtags = words.filter(word => word.startsWith("#"))
    val counts = hashtags.map(tag => (tag, 1))
      .reduceByKeyAndWindow(_ + _, _ - _, Seconds(60 * 5), Seconds(1))

    val sortedCounts = counts.map({ case(tag, count) => (count, tag) })
      .transform(rdd => rdd.sortByKey(ascending = false))
    sortedCounts.foreachRDD(rdd =>
      println("\nTop 10 hashtags:\n" + rdd.take(10).mkString("\n"))
    )

    ssc.start()
    ssc.awaitTermination()

  }
}
