import AssemblyKeys._ // put this at the top of the file

name := "spark-spikes"

version := "1.0"


scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.1.0" % "provided",
  "org.apache.spark" %% "spark-sql" % "1.1.0" % "provided",
  "org.apache.spark" %% "spark-hive" % "1.1.0" % "provided",
  "org.apache.spark" %% "spark-streaming" % "1.1.0" % "provided",
  "org.apache.spark" %% "spark-streaming-twitter" % "1.1.0"
)

resourceDirectory in Compile := baseDirectory.value / "resources"
// https://github.com/sbt/sbt-assembly

assemblySettings

mergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf")          => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$")      => MergeStrategy.discard
  case "log4j.properties"                                  => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case "reference.conf"                                    => MergeStrategy.concat
  case _                                                   => MergeStrategy.first
}